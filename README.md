# README #

### This repository created in order to make our research easily reproducible ###


### How do I get set up? ###

* Save all files of 01-01-2011 to directory tweets2011
* Save all files of 31-07-2016 to directory tweets2016
* Open a linux terminal
* Append all files in tweets2011 directory with code: 

cat *.out >> tweets2011.txt

* Append all files in tweets2016 directory with code: 

cat *.out >> tweets2016.txt

* Place both txt files in twitterdata directory
* Select the first 40000 lines in tweets2011.txt. Cut field 4 with the deliminator ':' from the result. 
* Remove duplicate lines from the result. In these lines select the case insensitive string 'hun', but leave out sub-word matches. 
* Count all occurences of 'hun'. To do this use code:

head -40000 tweets2011.txt | cut -d ':' -f4 | uniq | grep -iw 'hun' | wc -w

* Place both txt files in twitterdata directory
* Select the first 40000 lines in tweets2011.txt. Cut field 7 with the deliminator ':' from the result. 
* Remove duplicate lines from the result. In these lines select the case insensitive string 'hun', but leave out sub-word matches. 
* Count all occurences of 'hun'. To do this use code:

head -40000 tweets2016.txt | cut -d ':' -f7 | uniq | grep -iw 'hun' | wc -w